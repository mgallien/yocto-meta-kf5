# Class for recipes using ki18n for internationalization
inherit python3native
inherit gettext

DEPENDS += " \
    ki18n \
    gettext-native \
"
